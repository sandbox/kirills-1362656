=====
PayPal for Digital Goods
-----
This module provides API integration with the PayPal for Digital Goods (Express Checkout) is PayPal's solution for
buying and selling digital goods. With simple integration, it offers in-context payments within your digital content.


=====
Installation
-----

1. Enable the module
2. Set the options on settings page gateway